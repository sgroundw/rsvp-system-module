namespace Drupal\rsvp_qr_reaction_rule\Plugin\ECAReaction;

use Drupal\eca\Plugin\ECAReactionBase;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;
use Imagick;
use ImagickPixel;

/**
 * Provides a QR code generation reaction.
 *
 * @ECAReaction(
 *   id = "generate_qr_code",
 *   label = @Translation("Generate QR Code"),
 *   description = @Translation("Generates a QR code with custom formatting.")
 * )
 */
class GenerateQrCode extends ECAReactionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(array $context) {
    // Get data from the context or other sources.
    $data = 'https://example.com'; // Replace with dynamic data as needed.

    // Generate the QR code.
    $qrCode = new QrCode($data);
    $writer = new PngWriter();
    $qrCodeData = $writer->write($qrCode)->getString();

    // Use ImageMagick to apply additional formatting.
    $imagick = new Imagick();
    $imagick->readImageBlob($qrCodeData);
    // Apply ImageMagick transformations here, e.g., add a border.
    $imagick->borderImage(new ImagickPixel('black'), 10, 10);

    // Save or return the formatted QR code.
    $formattedQrCodeData = $imagick->getImageBlob();
    // Save to file, attach to node, etc.
    // Example: Save to a file.
    file_put_contents('public://qr_codes/qr_code.png', $formattedQrCodeData);
  }
}
