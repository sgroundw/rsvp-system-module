# RSVP System - Companion Moudule

RSVP System is an Open Source recipe cookbook for buidling calendars applications with Drupal. This is a companion module for application elements that are outside of the scope of what Drupal recipes delivers.  ECA Reaction Rules and Custom Views Formats are just examples of what this module intends to provide.

[RSVP System - Module](https://gitlab.com/rsvp-system/rsvp-system-module)

[RSVP System - Recipe Cookbook](https://gitlab.com/rsvp-system/rsvp-recipes)

## Project Components

### 1. RSVP System **Recipe Cookbook**

- Collection of Drupal Recipes for calendar-based systems.
- Pre-configured site components for reservations, event management, and resource booking.
- Community taxonomy system for content tagging and indexing based on organizational needs. (Affiliated Site Sections)

### 2. RSVP System Documentation

- Sample data sets for testing.
- Configuration exports for quick setup.
- Implementation notes

### 3. **Radix Sub-Theme**

- Custom Drupal theme based on Radix.
- Provides a lightweight, responsive UI.

### 4. **Companion Module**

- Extends Drupal functionality for RSVP System workflows.
- Includes ECA reaction plugins and content validation.
- Enhances reservations, approvals, and logging.
